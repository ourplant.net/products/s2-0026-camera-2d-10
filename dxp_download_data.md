Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0026-camera-2d-10).

|document|download options|
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s2-0026-camera-2d-10/-/raw/main/01_operating_manual/S2-0026_C_BA_Camera.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s2-0026-camera-2d-10/-/raw/main/02_assembly_drawing/s2-0026-C_ZNB_camera.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s2-0026-camera-2d-10/-/raw/main/03_circuit_diagram/S2-0026_C_EPlan.pdf)|
|maintenance instructions   |[de](https://gitlab.com/ourplant.net/products/s2-0026-camera-2d-10/-/raw/main/04_maintenance_instructions/S2-0026_B_WA_Camera.pdf)|
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s2-0026-camera-2d-10/-/raw/main/05_spare_parts/S2-0026_A1_EVL_Camera.pdf)|

<!-- 2021 (C) Häcker Automation GmbH -->
